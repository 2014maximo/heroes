import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService} from '../../services/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: []
})
export class HeroeComponent implements OnInit {

  heroe:any = {};

  constructor(private activatedRoute: ActivatedRoute,
              private _heroes_Service: HeroesService) {

    /* Recibe parametros(params) por url (activatedRoute) */
    this.activatedRoute.params.subscribe(params => {
      this.heroe = this._heroes_Service.getHeroe( params ['id']);
    });

  }

  ngOnInit() {
  }

}
